# Copyright 2018 Gontzal Gomez - AvanzOSC
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html

{
    "name": "Project Purchase Link",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "AvanzOSC, " "Odoo Community Association (OCA)",
    "depends": ["project", "purchase", "hr_timesheet"],
    "website": "https://gitlab.com/flectra-community/project",
    "category": "Project",
    "data": ["views/project_project_view.xml"],
    "installable": True,
    "maintainers": ["udesai", "OCA"],
}
