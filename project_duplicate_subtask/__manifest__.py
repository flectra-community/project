# Copyright (C) 2021 ForgeFlow S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html)

{
    "name": "Project Duplicate subtask",
    "version": "2.0.1.0.0",
    "category": "Project",
    "website": "https://gitlab.com/flectra-community/project",
    "summary": "The module adds an action to duplicate tasks with the child subtasks",
    "author": "Forgeflow, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["project"],
    "data": ["views/project_duplicate_action.xml"],
    "installable": True,
    "auto_install": False,
}
