# Copyright 2019-2020 Onestein
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Project Deadline",
    "summary": "Start date and deadline of projects.",
    "author": "Onestein, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/project",
    "category": "Project Management",
    "version": "2.0.1.0.0",
    "depends": ["project"],
    "data": ["views/project_project_view.xml", "views/project_task_view.xml"],
}
