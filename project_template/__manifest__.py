# Copyright 2019 Patrick Wilson <patrickraymondwilson@gmail.com>
# License LGPLv3.0 or later (https://www.gnu.org/licenses/lgpl-3.0.en.html).

{
    "name": "Project Templates",
    "summary": """Project Templates""",
    "author": "Patrick Wilson, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/project",
    "category": "Project Management",
    "version": "2.0.1.2.0",
    "license": "AGPL-3",
    "depends": ["project"],
    "data": ["views/project.xml"],
    "application": False,
    "development_status": "Beta",
    "maintainers": ["patrickrwilson"],
}
