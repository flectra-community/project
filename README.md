# Flectra Community / project

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[project_task_pull_request](project_task_pull_request/) | 2.0.1.1.0| Adds a field for a PR URI to project tasks
[project_stage_state](project_stage_state/) | 2.0.1.1.0| Restore State attribute removed from Project Stages in 8.0
[project_category](project_category/) | 2.0.1.0.0| Project Types
[project_wbs](project_wbs/) | 2.0.1.0.1| Apply Work Breakdown Structure
[project_key](project_key/) | 2.0.1.0.2| Module decorates projects and tasks with Project Key
[project_task_default_stage](project_task_default_stage/) | 2.0.1.1.0| Recovery default task stages for projects from v8
[project_stock](project_stock/) | 2.0.1.1.4| Project Stock
[project_stock_product_set](project_stock_product_set/) | 2.0.1.0.2| Project Stock Product Set
[project_task_code](project_task_code/) | 2.0.1.0.2| Sequential Code for Tasks
[project_timeline_hr_timesheet](project_timeline_hr_timesheet/) | 2.0.1.0.1| Shows the progress of tasks on the timeline view.
[project_timeline_task_dependency](project_timeline_task_dependency/) | 2.0.1.0.0| Render arrows between dependencies.
[project_task_send_by_mail](project_task_send_by_mail/) | 2.0.1.0.0| Send task report by email
[project_mail_chatter](project_mail_chatter/) | 2.0.1.1.0| Add message chatter on the Project form.
[project_task_material](project_task_material/) | 2.0.1.0.0| Record products spent in a Task
[project_gtd](project_gtd/) | 2.0.1.0.0| Personal Tasks, Contexts, Timeboxes
[project_milestone](project_milestone/) | 2.0.1.3.0| Project Milestones
[project_duplicate_subtask](project_duplicate_subtask/) | 2.0.1.0.0| The module adds an action to duplicate tasks with the child subtasks
[project_task_add_very_high](project_task_add_very_high/) | 2.0.1.1.0| Adds extra options 'High' and 'Very High' on tasks
[project_tag](project_tag/) | 2.0.1.1.0| Project Tags
[project_recalculate](project_recalculate/) | 2.0.1.0.0| Project Recalculate
[project_status](project_status/) | 2.0.1.0.0|         Project Status
[project_hr](project_hr/) | 2.0.1.0.0| Link HR with project
[project_role](project_role/) | 2.0.1.0.0| Project role-based roster
[project_template](project_template/) | 2.0.1.2.0| Project Templates
[project_purchase_link](project_purchase_link/) | 2.0.1.0.0| Project Purchase Link
[project_timeline](project_timeline/) | 2.0.1.5.0| Timeline view for projects
[project_deadline](project_deadline/) | 2.0.1.0.0| Start date and deadline of projects.
[project_task_digitized_signature](project_task_digitized_signature/) | 2.0.1.0.0| Project Task Digitized Signature
[project_task_recurring_activity](project_task_recurring_activity/) | 2.0.1.0.0| Project Task Recurring Activity
[project_list](project_list/) | 2.0.1.0.0| Projects list view
[project_stock_request](project_stock_request/) | 2.0.1.0.0| Create stock requests from a projects and project tasks
[project_timesheet_time_control_sale](project_timesheet_time_control_sale/) | 2.0.1.0.0| Make 'Project timesheet time control' and 'Sales Timesheet' work together
[project_parent_task_filter](project_parent_task_filter/) | 2.0.1.2.0| Add filters to show the parent or non parent tasks
[project_template_milestone](project_template_milestone/) | 2.0.1.0.0| Adds function to copy of milestones when creating                  a project from template
[project_task_dependency](project_task_dependency/) | 2.0.1.1.0| Enables to define dependencies (other tasks) of a task
[project_stage_closed](project_stage_closed/) | 2.0.2.0.0| Project Task Stage Closed
[project_sequence](project_sequence/) | 2.0.0.2.0| Add a sequence field to projects, filled automatically
[project_task_project_required](project_task_project_required/) | 2.0.1.0.0|         Set project on task as a mandatory field
[project_stage_mgmt](project_stage_mgmt/) | 2.0.1.0.0| Allows to assign and create stages on project creation wizard
[project_timesheet_time_control](project_timesheet_time_control/) | 2.0.1.1.1| Project timesheet time control


