# Copyright 2017 - 2018 Mflectralar <info@mflectralar.com>
# License LGPLv3.0 or later (https://www.gnu.org/licenses/lgpl-3.0.en.html).


def post_init_hook(cr, registry):
    from flectra import SUPERUSER_ID, api

    env = api.Environment(cr, SUPERUSER_ID, {})
    env["project.project"]._set_default_project_key()
